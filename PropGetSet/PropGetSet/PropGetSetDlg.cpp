
// PropGetSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PropGetSet.h"
#include "PropGetSetDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

///Added this for Code Review To Check

////

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CPropGetSetDlg dialog



CPropGetSetDlg::CPropGetSetDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPropGetSetDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPropGetSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edtVarName);
	DDX_Control(pDX, IDC_COMBO1, m_cboVarType);
	DDX_Control(pDX, IDC_COMBO2, m_cboXPropType);
}

BEGIN_MESSAGE_MAP(CPropGetSetDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CPropGetSetDlg::OnBnClickedGenerate)
END_MESSAGE_MAP()


// CPropGetSetDlg message handlers

BOOL CPropGetSetDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	FillVarType();
	FillXPropType();




	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPropGetSetDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPropGetSetDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPropGetSetDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CPropGetSetDlg::OnBnClickedGenerate()
{
	WriteVariablesintoFile();
	WriteSetMethodintoFile();
	WriteGetMethodintoFile();

	switch (m_cboXPropType.GetCurSel())
	{
	case XPropGetSet:
		WriteXPropGetSetintoFile();
		break;

	case XPropGet:
		WriteXPropGetintoFile();
		break;

	case XPropSet:
		WriteXPropSetintoFile();
		break;

	default:
		break;
	}
	m_edtVarName.SetWindowText(_T(""));
}

void CPropGetSetDlg::FillVarType()
{
	m_cboVarType.AddString(_T("CXInt"));
	m_cboVarType.AddString(_T("CXLong"));
	m_cboVarType.AddString(_T("CString"));
	m_cboVarType.AddString(_T("CXBool"));
	m_cboVarType.AddString(_T("CXByte"));
	m_cboVarType.AddString(_T("CXFloat"));

	m_cboVarType.SetCurSel(CxInt);
}

void CPropGetSetDlg::FillXPropType()
{
	m_cboXPropType.AddString(_T("XPropGetSet"));
	m_cboXPropType.AddString(_T("XPropGet"));
	m_cboXPropType.AddString(_T("XPropSet"));

	m_cboXPropType.SetCurSel(XPropGetSet);
}

void CPropGetSetDlg::WriteVariablesintoFile()
{
	CString strVarName;
	CStdioFile cFile;

	m_edtVarName.GetWindowText(strVarName);
	int nVarType = m_cboVarType.GetCurSel();

	if (cFile.Open(_T("Test\\VariablesFile.txt"), CFile::modeCreate | CFile::modeWrite | CFile::modeNoTruncate | CFile::typeText))
	{
		cFile.SeekToEnd();
		switch (nVarType)
		{
		case CxLong:
			cFile.WriteString(_T("CXLong\tm_l") + strVarName + _T("{};\n"));
			break;
		case Cstring:
			cFile.WriteString(_T("CString\tm_str") + strVarName + _T("{};\n"));
			break;
		case CxBool:
			cFile.WriteString(_T("CXBool\tm_b") + strVarName + _T("{};\n"));
			break;
		case CxByte:
			cFile.WriteString(_T("CXByte\tm_by") + strVarName + _T("{};\n"));
			break;
		case CxFloat:
			cFile.WriteString(_T("CXFloat\tm_d") + strVarName + _T("{};\n"));
			break;
		case CxInt:
			cFile.WriteString(_T("CXInt\tm_n") + strVarName + _T("{};\n"));
			break;
		}
		cFile.Close();
	}
}

void CPropGetSetDlg::WriteSetMethodintoFile()
{
	CString strVarName;
	CStdioFile cFile;

	m_edtVarName.GetWindowText(strVarName);
	int nVarType = m_cboVarType.GetCurSel();
	
	if (cFile.Open(_T("Test\\SetMethodsFile.txt"), CFile::modeCreate | CFile::modeWrite | CFile::modeNoTruncate | CFile::typeText))
	{
		cFile.SeekToEnd();
		switch (nVarType)
		{
		case CxLong:
			cFile.WriteString(_T("void\tset") + strVarName + _T("(") + _T("CXLong l") + strVarName + _T(") { m_l") + strVarName + _T(" = l") + strVarName + _T("; }\n"));
			break;
		case Cstring:
			cFile.WriteString(_T("void\tset") + strVarName + _T("(") + _T("CString str") + strVarName + _T(") { m_str") + strVarName + _T(" = str") + strVarName + _T("; }\n"));
			break;
		case CxBool:
			cFile.WriteString(_T("void\tset") + strVarName + _T("(") + _T("CXBool b") + strVarName + _T(") { m_b") + strVarName + _T(" = b") + strVarName + _T("; }\n"));
			break;
		case CxByte:
			cFile.WriteString(_T("void\tset") + strVarName + _T("(") + _T("CXByte by") + strVarName + _T(") { m_by") + strVarName + _T(" = by") + strVarName + _T("; }\n"));
			break;
		case CxFloat:
			cFile.WriteString(_T("void\tset") + strVarName + _T("(") + _T("CXFloat d") + strVarName + _T(") { m_d") + strVarName + _T(" = d") + strVarName + _T("; }\n"));
			break;
		case CxInt:
			cFile.WriteString(_T("void\tset") + strVarName + _T("(") + _T("CXInt n") + strVarName + _T(") { m_n") + strVarName + _T(" = n") + strVarName + _T("; }\n"));
			break;
		}
		cFile.Close();
	}
}

void CPropGetSetDlg::WriteGetMethodintoFile()
{
	CString strVarName;
	CStdioFile cFile;

	m_edtVarName.GetWindowText(strVarName);
	int nVarType = m_cboVarType.GetCurSel();
	
	if (cFile.Open(_T("Test\\GetMethodsFile.txt"), CFile::modeCreate | CFile::modeWrite | CFile::modeNoTruncate | CFile::typeText))
	{
		cFile.SeekToEnd();
		switch (nVarType)
		{
		case CxLong:
			cFile.WriteString(_T("CXLong\tget") + strVarName + _T("( ) { return  m_l") + strVarName + _T("; }\n"));
			break;
		case Cstring:
			cFile.WriteString(_T("CString\tget") + strVarName + _T("( ) { return  m_str") + strVarName + _T("; }\n"));
			break;
		case CxBool:
			cFile.WriteString(_T("CXBool\tget") + strVarName + _T("( ) { return m_b") + strVarName + _T("; }\n"));
			break;
		case CxByte:
			cFile.WriteString(_T("CXByte\tget") + strVarName + _T("( ) {return  m_by") + strVarName + _T("; }\n"));
			break;
		case CxFloat:
			cFile.WriteString(_T("CXFloat\tget") + strVarName + _T("( ) {return  m_d") + strVarName + _T("; }\n"));
			break;
		case CxInt:
			cFile.WriteString(_T("CXInt\tget") + strVarName + _T("( ) { return m_n") + strVarName + _T("; }\n"));
			break;
		}
		cFile.Close();
	}
}

void CPropGetSetDlg::WriteXPropGetSetintoFile()
{
	CString strVarName;
	CStdioFile cFile;

	m_edtVarName.GetWindowText(strVarName);
	int nVarType = m_cboVarType.GetCurSel();

	if (cFile.Open(_T("Test\\XPropGetSetFile.txt"), CFile::modeCreate | CFile::modeWrite | CFile::modeNoTruncate | CFile::typeText))
	{
		cFile.SeekToEnd();
		switch (nVarType)
		{
		case CxLong:
			cFile.WriteString(_T("XPROPGETSET(") + strVarName + _T(", CXLong, get") + strVarName + _T(", set") + strVarName + _T(");\n"));
			break;
		case Cstring:
			cFile.WriteString(_T("XPROPGETSET(") + strVarName + _T(", CString, get") + strVarName + _T(", set") + strVarName + _T(");\n"));
			break;
		case CxBool:
			cFile.WriteString(_T("XPROPGETSET(") + strVarName + _T(", CXBool, get") + strVarName + _T(", set") + strVarName + _T(");\n"));
			break;
		case CxByte:
			cFile.WriteString(_T("XPROPGETSET(") + strVarName + _T(", CXByte, get") + strVarName + _T(", set") + strVarName + _T(");\n"));
			break;
		case CxFloat:
			cFile.WriteString(_T("XPROPGETSET(") + strVarName + _T(", CXFloat, get") + strVarName + _T(", set") + strVarName + _T(");\n"));
			break;
		case CxInt:
			cFile.WriteString(_T("XPROPGETSET(") + strVarName + _T(", CXInt, get") + strVarName + _T(", set") + strVarName + _T(");\n"));
			break;
		}
		cFile.Close();
	}
}

void CPropGetSetDlg::WriteXPropGetintoFile()
{
	CString strVarName;
	CStdioFile cFile;

	m_edtVarName.GetWindowText(strVarName);
	int nVarType = m_cboVarType.GetCurSel();

	if (cFile.Open(_T("Test\\XPropGetSetFile.txt"), CFile::modeCreate | CFile::modeWrite | CFile::modeNoTruncate | CFile::typeText))
	{
		cFile.SeekToEnd();
		switch (nVarType)
		{
		case CxLong:
			cFile.WriteString(_T("XPROPGET(") + strVarName + _T(", CXLong, get") + strVarName + _T(");\n"));
			break;
		case Cstring:
			cFile.WriteString(_T("XPROPGET(") + strVarName + _T(", CString, get") + strVarName + _T(");\n"));
			break;
		case CxBool:
			cFile.WriteString(_T("XPROPGET(") + strVarName + _T(", CXBool, get") + strVarName + _T(");\n"));
			break;
		case CxByte:
			cFile.WriteString(_T("XPROPGET(") + strVarName + _T(", CXByte, get") + strVarName + _T(");\n"));
			break;
		case CxFloat:
			cFile.WriteString(_T("XPROPGET(") + strVarName + _T(", CXFloat, get") + strVarName + _T(");\n"));
			break;
		case CxInt:
			cFile.WriteString(_T("XPROPGET(") + strVarName + _T(", CXInt, get") + strVarName + _T(");\n"));
			break;
		}
		cFile.Close();
	}
}

void CPropGetSetDlg::WriteXPropSetintoFile()
{
	CString strVarName;
	CStdioFile cFile;

	m_edtVarName.GetWindowText(strVarName);
	int nVarType = m_cboVarType.GetCurSel();
	
	if (cFile.Open(_T("Test\\XPropGetSetFile.txt"), CFile::modeCreate | CFile::modeWrite | CFile::modeNoTruncate | CFile::typeText))
	{
		cFile.SeekToEnd();
		switch (nVarType)
		{
		case CxLong:
			cFile.WriteString(_T("XPROPSET(") + strVarName + _T(", CXLong, set") + strVarName + _T(");\n"));
			break;
		case Cstring:
			cFile.WriteString(_T("XPROPSET(") + strVarName + _T(", CString, set") + strVarName + _T(");\n"));
			break;
		case CxBool:
			cFile.WriteString(_T("XPROPSET(") + strVarName + _T(", CXBool, set") + strVarName + _T(");\n"));
			break;
		case CxByte:
			cFile.WriteString(_T("XPROPSET(") + strVarName + _T(", CXByte, set") + strVarName + _T(");\n"));
			break;
		case CxFloat:
			cFile.WriteString(_T("XPROPSET(") + strVarName + _T(", CXFloat, set") + strVarName + _T(");\n"));
			break;
		case CxInt:
			cFile.WriteString(_T("XPROPSET(") + strVarName + _T(", CXInt, set") + strVarName + _T(");\n"));
			break;
		}
		cFile.Close();
	}
}