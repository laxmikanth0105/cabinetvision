
// PropGetSetDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CPropGetSetDlg dialog
class CPropGetSetDlg : public CDialogEx
{
// Construction
public:
	CPropGetSetDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_PROPGETSET_DIALOG };

	enum varType
	{
		CxInt = 0,
		CxLong,
		Cstring,
		CxBool,
		CxByte,
		CxFloat,
	};

	enum XPropType
	{
		XPropGetSet = 0,
		XPropGet,
		XPropSet,
	};

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
	void	FillVarType();
	void	FillXPropType();
	void WriteVariablesintoFile();
	void WriteSetMethodintoFile();
	void WriteGetMethodintoFile();
	void WriteXPropGetSetintoFile();
	void WriteXPropGetintoFile();
	void WriteXPropSetintoFile();

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedGenerate();
	CEdit m_edtVarName;
	CComboBox m_cboVarType;
	CComboBox m_cboXPropType;
	CStdioFile cFile;
	CString strVarFile, strGetterFile, strSetterFile, strXPROFile;

};
